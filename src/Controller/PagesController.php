<?php

namespace App\Controller;

use App\Repository\AuthorRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index( AuthorRepository $authorRepository)
    {

        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $this->getDoctrine()->getManager();

        $ids = ['11', '12', '13'];
        $conn = $entityManager->getConnection();
//        $stmt = $conn->prepare("
//            select * from author where id in (:ids);
//        ");
//
//        $stmt->bindValue('ids', $ids);
//
//        $stmt->execute();
//        $result = $stmt->fetchAll();

        $result = null;

//        try{
//            $result = $conn->executeQuery("select * from author where address = ? or id in (?) ", ['bankai', $ids ], [null, Connection::PARAM_STR_ARRAY])
//            ->fetchAll();
//        }catch (\Exception $exception){
//            dump($exception->getMessage());
//            die;
//        }

        try{
            $result = $conn->executeUpdate("update author set address = (?) where id in (?)", [['loom', 'minn', 'nate'], ['13', '14', '20']], [Connection::PARAM_STR_ARRAY, Connection::PARAM_STR_ARRAY]);
        }catch (Exception $exception){
            dump($exception->getMessage());
            die;
        }

//        $authors = $authorRepository->findBy(['id' => ['13', '14', '16', '17']]);

//        $authors = $authorRepository->findByCustom(['13', '14']);
//        $authors = $authorRepository->updateMultiple(['13', '14']);

        dump($result);
        die;

        return new Response('bla');
    }
}
