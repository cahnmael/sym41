<?php


namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

abstract class BaseFixture extends Fixture {


    /**
     * @var ObjectManager
     */
    protected $manager;


    /**
     * @var Generator
     */
    protected $faker;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $this->faker = Factory::create();

        $this->manager = $manager;

        $this->loadData($manager);

    }

    abstract protected function loadData(ObjectManager $em);

    protected function createMany(string $className, int $count, callable $factory)

    {
        for ($i = 0; $i < $count; $i++) {

            $entity = new $className();

            $factory($entity, $i);

            $this->manager->persist($entity);

            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className . '_' . $i, $entity);
        }
    }
}