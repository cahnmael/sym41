<?php

namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;

class AuthorFixture extends BaseFixture
{

    protected function loadData(ObjectManager $em)
    {

        $this->createMany(Author::class, 10, function ($author, $index){

            /**
             * @var Author $author
             */
            $author->setEmail($this->faker->email)
                ->setAddress($this->faker->address)
                ->setName($this->faker->name);
        });

        $em->flush();
    }
}
