<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixture extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Post::class, 100, function ($post){

            /**
             * @var Post $post
             */
            $post->setTitle($this->faker->sentence(5))
                ->setBody($this->faker->paragraph)
                ->setAuthor($this->getReference(Author::class . '_0'));


        });

        $manager->flush();
    }

//    /**
//     * This method must return an array of fixtures classes
//     * on which the implementing class depends on
//     *
//     * @return array
//     */
//    public function getDependencies()
//    {
//        return [
//            Author::class,
//        ];
//    }
}
