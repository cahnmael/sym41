<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Author::class);
    }


    public function findByCustom($id)
    {
       $qb =  $this->createQueryBuilder('a');

//       return    $qb->add('where', $qb->expr()->in('a.id', ['13', '14']))
////           $qb->setParameter(1, ['13' ,'14'], Connection::PARAM_STR_ARRAY)
//            ->getQuery()
//            ->getResult();

//        return    $qb->add('where', $qb->expr()->in('a.id', '?1'))
//           ->setParameter(1, ['13' ,'14'], Connection::PARAM_STR_ARRAY)
//            ->getQuery()
//            ->getResult();

//        return    $qb->add('where', $qb->expr()->in('a.id', ':ids'))
//            ->setParameter('ids', ['13' ,'14'])
//            ->getQuery()
//            ->getResult();

//        return $this->createQueryBuilder('a')
//            ->andWhere('a.id in (:id)')
//            ->setParameter('id',$ids)
//            ->getQuery()
//            ->getResult();

//        return $this->getEntityManager()->createQuery('select a from App\Entity\Author a where a.id = 15')->getResult();

//        return $this->getEntityManager()->createQuery('select a from App\Entity\Author a where a.id in (:ids)')
//            ->setParameter('ids', ['14', '15'])->getResult();


        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from('App\Entity\Author', 'a')
            ->andWhere('a.id in (:ids)')
            ->setParameter('ids', ['15', '16', '17'])
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        return    $qb->add('where', $qb->expr()->in('a.id', '?1'))
           ->setParameter(1, ['13' ,'14'], Connection::PARAM_STR_ARRAY)
            ->getQuery()
            ->getResult();
    }

    public function updateMultiple(array $ids)
    {


        return $this->getEntityManager()->createQuery("update App\\Entity\\Author a set a.address = 'bankai' where a.id in (:ids)")
            ->setParameter('ids', ['14', '15'])->getResult();

//        return $this->getEntityManager()->createQuery('select a from App\Entity\Author a where a.id in (:ids)')
//            ->setParameter('ids', ['14', '15'])->getResult();
//
//
//        $qb =$this->getEntityManager()->createQueryBuilder();



//        return $qb->update('App\Entity\Author', 'a')
//            ->andWhere('a.id in (:ids)')
//            ->setParameter('ids', $ids)
//            ->set('a.address', $qb->expr()->literal('Arau'))
//            ->getQuery()
//            ->getResult();


    }


//    /**
//     * @return Author[] Returns an array of Author objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Author
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
